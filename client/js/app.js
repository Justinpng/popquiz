/**
 * Client side code.
 */
(function() {
    "use strict"; //always include this after IIFE, so that it will remind you of missing special characters
    var app = angular.module("PopQuizApp", []); //naming this as PopQuiz App

    app.controller("PopQuizCtrl", ["$http", PopQuizCtrl]); //naming PopQuizCtrl as the controller

    /* Creating the PopQuiz app */
    function PopQuizCtrl($http) {
        var self = this; // vm

        self.quiz = { //leaving this blank because it wil be pulling inputs from user http
            
        };

        /* Blank because it will be capturing user input and save it under final answer array*/
        self.finalanswer = {
            id: 0,
            value: "",
            comments: "",
            message: ""
        };
/* This function is to display the question (after running the random) */
        self.initForm = function() {
            $http.get("/popquizes")
            .then(function(result) {
                console.log(result);
                self.quiz = result.data;
            }).catch(function(e) {
                console.log(e);
            });
        }

        self.initForm(); // to activate this function everytime a refresh is trigger so it will regenerate a new set of question

        /* Caturing the input from users and record it as finalanswer array */
        self.submitQuiz = function() {
            console.log("submitQuiz !!!");
            self.finalanswer.id = self.quiz.id;
            $http.post("/submit-quiz", self.finalanswer)
                .then(function(result) {
                    console.log(result);
                    /* Comparing input with define correct answer */
                    if(result.data.isCorrect){
                        self.finalanswer.message="It's CORRECT !"
                    }else{
                        self.finalanswer.message="WRONG !"
                    }
                }).catch(function(e) {
                    console.log(e);
                });
        };
    }

})();