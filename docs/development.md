## SETTING UP
1. Refer to README.md for INSTRUCTIONS 

## SERVER SIDE JS
1. CREATE THE ARRAY var quizes 
1. DEFINE THE ARRAY ATTRIBUTES (SEE EXAMPLE BELOW) REMEMBER TO INDICATE THE VALUE FOR THE CORRECT ANSWER FOR INPUT VARIFICATION LATER
```node
 [{
    id: 0, 
    question: "Question 1?",
    answer1: {name: "Answer 1", value: 1},
    answer2: {name: "Answer 2", value: 2},
    answer3: {name: "Answer 3" , value: 3},
    answer4: {name: "Answer 4", value: 4},
    answer5: {name: "Answer 5", value: 5},
    correctanswer: 1    
},]
```
1. CREATE A FUNCTION TO RANDOMISE THE QUESTIONS ARRAY
```node
    app.get("/popquizes", function(req, res) {
    var x = Math.random() * (5 - 1) + 1; 
    var y = Math.floor(x); 
    console.log(y);
    res.json(quizes[y - 1]);  
});
```
1. CREATE THE FUNCTION TO CAPTURE THE VALUES THAT USERS SUBMITTED (INCLUDES VARIFICATION ON CORRECT VALUE)
```node
    app.post("/submit-quiz", function(req, res) {
    console.log("Received user object " + req.body);
    console.log("Received user object " + JSON.stringify(req.body));
    var quiz = req.body; //capturing user inputs
    var checking = quizes[quiz.id]; //comparing user input with the correct answer 
    if (checking.correctanswer == parseInt(quiz.value)){
        console.log("CORRECT !");
        quiz.isCorrect = true;
    }else{
        console.log("INCORRECT !");
        quiz.isCorrect = false;
    }
    res.status(200).json(quiz); //return a check to server to indicate the function running successfully 
});
```

## CLIENT SIDE JS & CODING THE HTML
1. APP.JS --> DEFINE IIFE: ANGULAR MODULE ``` (ng-app=PopQuizApp)``` & APP CONTROLLER ``` (ng-controller)```
    1. Add in this line ``` <<"use strict";>>``` so that it will remind you of missing special characters
    1. NAMING THE APP: ``` var app = angular.module("PopQuizApp", []); ```
    1. NAMING THE CONTROLLER: ``` app.controller("PopQuizCtrl", ["$http", PopQuizCtrl]);```
1. HTML --> INITALISING THE HTML UNDER THE HTML-tag  ```ng-app=PopQuizApp ```
1. APP.JS --> CREATING THE FUNCTIONALITY FOR THE POP QUIZ APP 
```
    1.  function PopQuizCtrl($http) {
        var self = this;
        self.quiz = { 
        };
```
    1. DEFINING THE FINAL ANSWER ARRAY STRUCTURE
```
        self.finalanswer = {
            id: 0,
            value: "",
            comments: "",
            message: ""
        };
```
    1. CREATE THE FUNCTION TO RANDOMISE THE QUIZ QUESTIONS
```
        self.initForm = function() {
            $http.get("/popquizes")
            .then(function(result) {
                console.log(result);
                self.quiz = result.data;
            }).catch(function(e) {
                console.log(e);
            });
        }
```
    1. ADD THE BELOW FUNCTION TO ENSURE THAT THE RANDOM FUNCTION RE-INITATE WHEN USER REFRESHES THE APP
        ```self.initForm(); ```
        
1. HTML --> DIRECTING THE WEB COMPONENTS ON BROWSER
    1. DEFINING ng-controller AS THE DIRECTIVE ``` <div ng-controller="PopQuizCtrl as ctrl" class="col-sm-8 col-lg-4 popquiz-page"> ```
    1. DEFINING THE FORM NAME AND THE CONTROLLER FOR ng-submit 
    ```<form name="popquizForm" ng-submit="ctrl.submitQuiz()" novalidate> ```
    
    1. DISPLAYING THE RESULTS OF THE RANDOMISE QUESTION INSIDE THE FORM FIELD 
  ```<label for="quiz">{{ctrl.quiz.question}}</label> ```
    1.  DISPLAYING THE QUIZ ANSWERS IN FORM GROUP WITH ng-model TO CAPTURE THE INPUT ANSWER. INCLUDING THE VARIFICATION OF THE CORRECT ANSWER.
        ```<label for="answer">Answers : </label> <!-- Displaying the Quiz answers on browser with ng-model to capture the input answer-->
        <br/>
        <input id="answer" name="answer" type="radio" ng-model="ctrl.finalanswer.value" value="{{ctrl.quiz.answer1.value}}" /> {{ctrl.quiz.answer1.name}}
        ```
    1. CREATING THE SUBMIT BUTTON TO INITATE THE INPUT SUBMIT AND ANSWER COMPARING FUNCTION
      ```<input id="submit" name="submit" type="submit" class="btn btn-success" /> {{ ctrl.finalanswer.message}} ```
      
1. APP.JS --> TO CAPTURE THE INPUT AND VERIFY THE INPUT WITH THE CORRECT ANSWER
    ```
            self.submitQuiz = function() {
            console.log("submitQuiz !!!");
            self.finalanswer.id = self.quiz.id;
            $http.post("/submit-quiz", self.finalanswer)
                .then(function(result) {
                    console.log(result);
                    if(result.data.isCorrect){
                        self.finalanswer.message="It's CORRECT !"
                    }else{
                        self.finalanswer.message="WRONG !"
                    }
                }).catch(function(e) {
                    console.log(e);
                });
             };
```

## RUNNING THE WEB APP ON CHROME BROWSER
1. OPEN CHROME BROWSER
1. RUN THE APP ON URL: ``` http://localhost:3000/ ```
1. SELECT THE ANSWER AND TYPE IN THE COMMENT
1. REFRESH THE PAGE TO GENERATE QUESTIONS RANDOMLY
1. INPUT THE ANSWER AND COMMENT
1. APP WILL RESPONSE IF YOUR ANSWER IS RIGHT OR WRONG


