/**
 * Server side code.
 */
"use strict"; //always put this command line as this will help to check on syntex error
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.PORT || 3000; //Need to assign the port number 

app.use(express.static(__dirname + "/../client/"));

/* Creating the array in server script by hardcoding, usually this section will be extracted from database */
var quizes = [{
    id: 0, //question ID tag, function will select question based on this tag
    question: "What is the difference between HTML and CSS?",
    answer1: {name: "HTML deals with the function of the site and CSS the form", value: 1},
    answer2: {name: "CSS is a markup language unlike HTML", value: 2},
    answer3: {name: "CSS deals with the function of the site and HTML the form" , value: 3},
    answer4: {name: "CSS is a server language unlike HTML", value: 4},
    answer5: {name: "There is no difference", value: 5},
    correctanswer: 1    //Tagging the value for correct answer for True/False checking when user enter the selection
},
{
    id: 1,
    question: "What is the function of <title> This tag </title>",
    answer1: {name: "store meta information about the title", value: 1},
    answer2: {name: "show the server how to index the website", value: 2},
    answer3: {name: "show the title of the websites" , value: 3},
    answer4: {name: "There is no difference", value: 4},
    answer5: {name: "stores the title of the website for the browser and search engine results", value: 5},
    correctanswer: 5    
},
{
    id: 2,
    question: "What is the purpose of indentation in HTML?",
    answer1: {name: "it makes the code more readable", value: 1},
    answer2: {name: "HTML is whitespace dependant", value: 2},
    answer3: {name: "it is required in HTML’s syntax" , value: 3},
    answer4: {name: "HTML has to be done this way", value: 4},
    answer5: {name: "none", value: 5},
    correctanswer: 1    
},
{
    id: 3,
    question: "Inside table elements, what does text-align style?",
    answer1: {name: "text", value: 1},
    answer2: {name: "images", value: 2},
    answer3: {name: "buttons" , value: 3},
    answer4: {name: "divs", value: 4},
    answer5: {name: "tables", value: 5},
    correctanswer: 1    
},
{
    id: 4,
    question: "Why is it good practice to write comments in your code?",
    answer1: {name: "to understand your code when you come back to it.", value: 1},
    answer2: {name: "for others to understand your code.", value: 2},
    answer3: {name: "to make code more readable" , value: 3},
    answer4: {name: "all of the above", value: 4},
    answer5: {name: "none of the above", value: 5},
    correctanswer: 4    
}
];

/* Randomise function to generate the questions randomly */
app.get("/popquizes", function(req, res) {
    
    var x = Math.random() * (6 - 1) + 1; //using math.random to random the questions number, need to put 6 because we round down the number
    var y = Math.floor(x); //rounding off the question number down to a full number
    console.log(y);
    res.json(quizes[y - 1]); //NOTE: need to have the minus 1 so that question ID 0, will be called too 
});

/* This section is to capture the results when users click submit button*/
app.post("/submit-quiz", function(req, res) {
    console.log("Received user object " + req.body);
    console.log("Received user object " + JSON.stringify(req.body));
    var quiz = req.body; //capturing user inputs
    var checking = quizes[quiz.id]; //comparing user input with the correct answer 
    if (checking.correctanswer == parseInt(quiz.value)){
        console.log("CORRECT !");
        quiz.isCorrect = true;
    }else{
        console.log("INCORRECT !");
        quiz.isCorrect = false;
    }
    res.status(200).json(quiz); //return a check to server to indicate the function running successfully 
});

/* Checking User URL input*/
app.use(function(req, res) {
    res.send("<h1>!!!! Page not found ! ! </h1>");
});

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});